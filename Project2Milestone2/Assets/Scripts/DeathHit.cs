﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathHit : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D collision) //run function when two sprites collide
    {
        if (this.gameObject.tag != collision.gameObject.tag) //trigger if the player and an enemy collide but not if two enemies collide
        {
            Destroy(this.gameObject); //destroy the gameObject that this script is attached to
        }        
    }
}
