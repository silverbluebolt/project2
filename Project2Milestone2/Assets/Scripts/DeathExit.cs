﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathExit : MonoBehaviour {

    private void OnTriggerExit2D(Collider2D collision) //run function when a sprite leaves the play area
    {
        Destroy(collision.gameObject); //destroy the gameObject that left the trigger 
    }
}
