﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {
	private Transform tf; //create a variable to hold a transform component
	public float speed; //create a variable for linear speed
	public int rSpeed; //create a variable for rotation speed

	// Use this for initialization
	void Start () {
        //initialize the variables
		tf = GetComponent<Transform>();
        speed = 0.3f;
        rSpeed = 3;
	}
	
	// Update is called once per frame
	void Update () {
        //linear and rotational movement are in two seperate if, else statements to allow both at the same time
		if (Input.GetKey (KeyCode.LeftArrow)) { //when the left arrow is pressed, rotate counter-clockwise
			tf.Rotate (0, 0, rSpeed);
		} else if (Input.GetKey (KeyCode.RightArrow)) { //when the right arrow is pressed, rotate clockwise
			tf.Rotate (0, 0, -rSpeed);
		}

		if (Input.GetKey (KeyCode.UpArrow)) { //when the up arrow is pressed, move forward
			tf.position += tf.right * speed;
		} else if (Input.GetKey (KeyCode.DownArrow)) { //when the down arrow is pressed, move backwards
			tf.position -= tf.right * speed;
		}        
	}
}
