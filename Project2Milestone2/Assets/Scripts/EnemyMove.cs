﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {

    private Transform tf; //create a variable to hold a transform component
    private int counter; //create a variable that is used as a counter
    private int lastDir; //create a variable that stores the last direction moved in as an int
    public float speed; //create a variable that determines speed

	// Use this for initialization
	void Start () {
        //initailize all variables to starting value
        tf = GetComponent<Transform>();
        counter = 0;
        lastDir = 0;
        speed = 0.4f;
	}
	
	// Update is called once per frame
	void Update () {
		if (counter < 40) //while counter is below this value...
        {
            tf.position += tf.right * speed; //change position according to direction and speed 
            counter++; //increment the counter 
        } else //when the counter gets too high...
        {
            tf.right = GetNewDirection(lastDir); //change direction
            counter = 0; //reset the counter
        }
	}

    Vector3 GetNewDirection(int lastDir) //funtion that changes the direction that a sprite is moving in
    {
        int dir = Random.Range(0, 8); //generate a random int from 0 to 7
        while (dir == lastDir) //re-generate number until it is different from the last one
        {
            dir = Random.Range(0, 8);
        }
        
        switch (dir)
        {
            
            case 0: //if the number is zero, move up
                lastDir = 0;
                return Vector3.up;

            case 1: //if the number is one, move right
                lastDir = 1; //set the last direction to the current direction
                return Vector3.right;

            case 2: //if the number is two, move down
                lastDir = 2; //set the last direction to the current direction
                return Vector3.down;

            case 3: //if the number is three, move left
                lastDir = 3; //set the last direction to the current direction
                return Vector3.left;

            case 4: //if the number is four, move up-right
                lastDir = 4; //set the last direction to the current direction
                Vector3 move = new Vector3(1, 1, 0); //create a new vector3 called move and set move to a value
                return move.normalized; //normalize the vector before returning it

            case 5: //if the number is five, move down-right
                lastDir = 5; //set the last direction to the current direction
                move = new Vector3(-1, 1, 0); //set move to a value
                return move.normalized; //normalize the vector before returning it

            case 6: //if the number is six, move down-left
                lastDir = 6; //set the last direction to the current direction
                move = new Vector3(-1, -1, 0); //set move to a value
                return move.normalized; //normalize the vector before returning it

            case 7: //if the number is seven, move up-left
                lastDir = 7; //set the last direction to the current direction
                move = new Vector3(1, -1, 0); //set move to a value
                return move.normalized; //normalize the vector before returning it

            default: //default to moving right
                return Vector3.right;                
        }
    }
}
