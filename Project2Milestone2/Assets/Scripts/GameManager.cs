﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	public static GameManager gm; //create a variable to hold the gameManager

	void awake(){
		if (gm == null) {
			gm = this; //store THIS instance of the class in the gm variable
			DontDestroyOnLoad (gameObject); //don't delete this object if a new scene is loaded
		} else {
			Destroy (this.gameObject); //destroy any new instances of this gameObject
		}
	}
}
