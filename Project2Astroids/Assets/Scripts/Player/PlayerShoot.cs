﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerShoot : MonoBehaviour {
    private Transform tf; //variable to hold a transform component
    public GameObject bullet; //variable to hold the bullet prefab

	// Use this for initialization
	void Start () {
        //initailize variables
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space)) { //if space is pressed, create a bullet
            Instantiate(bullet, tf.position, tf.rotation);
        }
	}
}

