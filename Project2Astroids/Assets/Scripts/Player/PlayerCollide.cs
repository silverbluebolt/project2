﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollide : MonoBehaviour {

    private GameObject SZ; //variable to hold the spawnZone

	// Use this for initialization
	void Start () {
        //initialize variables
        SZ = GameObject.Find("SpawnZone");
	}

    public void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Shard") //if hit by an enemy or a shard,
        {
            if (SZ.GetComponent<SpawnInvinciblilty>().invincible == false) //and if not invincible
            {
                GMScript.God.PlayerDeath(); //run death script
            }            
        }        
    }
}
