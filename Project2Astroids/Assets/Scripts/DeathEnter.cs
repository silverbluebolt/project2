﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathEnter : MonoBehaviour {
    
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            GMScript.God.PlayerDeath(); //if a player leaves the area, activate this function
        } else if (collision.gameObject.tag == "Enemy") {
            Destroy(collision.gameObject); //destroy the object that leaves the area
            GMScript.God.enemies--; //decriment the enemies counter 
        } else {
            Destroy(collision.gameObject); //destroy bullets and shards that leave the area
        }
    }
}
