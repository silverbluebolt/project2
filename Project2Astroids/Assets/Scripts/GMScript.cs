﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GMScript : MonoBehaviour {

    public static GMScript God; //variable to hold the GameManger Script
    public GameObject playerP; //variable that holds a permanent copy of the player sprite
    public GameObject playerT; //variable that holds the player copies that are destroyed
    public GameObject astroid; //variable that holds the astroid prefab
    public GameObject enemy; //variable that holds the enemy prefab
    private GameObject scoreNum; //variable that holds the ScoreNumber textbox
    private GameObject livesNum; //variable that holds the LivesNumber textbox
    public int score; //variable used to keep track of score
    public int lives; //variable used to keep track of lives
    public int enemies; //variable used to keep track of the number of enemies
    private Vector3 position; //variable to hold a random spawn point
    private int[] EList; //holds a list of ints that represent enemy types to be chosen at random
    private GameObject[] OList; //holds a list of enemies in the scene
     


	// Use this for initialization
	void Start () {
        Instantiate(playerP); //spawn the player

        //initialize variables that are not initialized in the editor
        God = this;
        playerT = GameObject.Find("PlayerShip(Clone)");
        scoreNum = GameObject.Find("ScoreNumber");       
        livesNum = GameObject.Find("LivesNumber");
        score = 0;
        lives = 3;
        livesNum.GetComponent<Text>().text = lives.ToString();
        enemies = 0;
        OList = GameObject.FindGameObjectsWithTag("Enemy");
        EList = new int[10] { 0, 0, 1, 0, 0, 1, 0, 0, 0, 1};
    }
	
	// Update is called once per frame
	void Update () {
        if (playerT == null) { //if the player doesn't exist
                Instantiate(playerP); //spawn a new player
                playerT = GameObject.Find("PlayerShip(Clone)"); //reset playerT to the new instance
        }
        EnemyCheck(); //run function to check the number of enemies
	}

    public void PlayerDeath() { //things that happen when the player dies
        lives--; //subtract one from the lives counter
        livesNum.GetComponent<Text>().text = lives.ToString(); //update the textbox to display the correct number of lives
        if (lives == 0) { //if the player runs out of lives, quit the game
            Application.Quit();//end game function
        }
        Destroy(playerT); //destroy the current instance of the player 
    }

    public void IncreaseScore() { 
        score++; //add one to the score counter
        scoreNum.GetComponent<Text>().text = score.ToString(); //update the textbox to display the correct score
    }

    private void EnemyCheck() { //checks the number of enemies, spawns enemies if there are less than 3 and destroys extras
        OList = GameObject.FindGameObjectsWithTag("Enemy"); //get an array of all enemies in the scene
        if (OList.Length > 3) { //check to see if there are more than three enemies
            int kill = OList.Length - 3; //find the number of extra enemies
            for (int i = 0; i < kill; i++) { //kill off extra enemies
                GameObject temp = OList[i];
                Destroy(temp);
            }
        }
        while (enemies < 3) { //spawn enemies if there are less than three
            int Enum = Random.Range(0, 10); //random number to create an enemy
            int Pnum = Random.Range(0, 10); //random number to find a spawn point 
            switch (Pnum) //switch to find a spawn point based off of an int
            {
                case 0:
                    position = new Vector3(35, 25);
                    break;
                case 1:
                    position = new Vector3(-35, 25);
                    break;
                case 2:
                    position = new Vector3(35, -25);
                    break;
                case 3:
                    position = new Vector3(-35, -25);
                    break;
                case 4:
                    position = new Vector3(0, 25);
                    break;
                case 5:
                    position = new Vector3(35, 0);
                    break;
                case 6:
                    position = new Vector3(0, -25);
                    break;
                case 7:
                    position = new Vector3(-35, 0);
                    break;
                case 8:
                    position = new Vector3(-27, 9);
                    break;
                case 9:
                    position = new Vector3(15, -16);
                    break;
                default:
                    //do nothing for default
                    break;
            }

            Quaternion q = new Quaternion(); //create a default quaternion
            Enum = EList[Enum]; //use the random number to find an enemy type

            if (Enum == 0) { //if the type is zero, generate an astroid
                Instantiate(astroid, position, q);
            } else if (Enum == 1) { //if the type is one, generate an enemy
                Instantiate(enemy, position, q);
            }
            enemies++; //increase the enemy counter
        }
    }
}
