﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnInvinciblilty : MonoBehaviour {
    
    public GameObject player; //variable to hold the player object
    public bool invincible; //variable to determine invincibility

    // Use this for initialization
    void Start () {
        //initialize variables
        invincible = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (player == null) { //when the player dies and is respawned
            player = GameObject.Find("PlayerShip(Clone)"); //set player to new instance
            invincible = true; //set invincible to true
        }

		if (Input.GetKeyDown(KeyCode.Space) && invincible == true) { 
            invincible = false; //set invicibility to false if the player fires a bullet
        }
	}

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") { //when the player leaves the trigger, set invicible to false;
            invincible = false;
        }
    }
}
