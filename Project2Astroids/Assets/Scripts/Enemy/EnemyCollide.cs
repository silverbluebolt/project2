﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollide : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Bullet") { //when hit by a bullet
            GMScript.God.IncreaseScore(); //increase score
            GMScript.God.enemies--; //decrease enemies counter
            Destroy(this.gameObject); //destroy this object
        } else if (collision.gameObject.tag == "Player") { //when hit by the player
            GMScript.God.enemies--; //decrease enemies counter
            Destroy(this.gameObject); //destroy this object
        }
    }
}
