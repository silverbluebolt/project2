﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour {

    private Transform tf; //used to store transform component for position and movement
    private GameObject player; //used to store the player's gameObject and get position
    private Vector3 direction; //used to control the direction of movement and orientation of the sprite
    public float speed; //used to control linear speed

    // Use this for initialization
    void Start () {
        //initailize variables
        tf = GetComponent<Transform>();
        speed = 0.5f;
        player = GameObject.Find("PlayerShip(Clone)");
        direction = player.transform.right; //direction to move is the same as the direction that the player is facing

        Destroy(this.gameObject, 2); //destroy this gameObject in 2 seconds after creation 
	}
	
	// Update is called once per frame
	void Update () {
        if (player == null) {
            Destroy(this.gameObject); //destroy this object if the player is destroyed
        }
        tf.position += direction * speed; //move in direction at the speed chosen
    }
}
