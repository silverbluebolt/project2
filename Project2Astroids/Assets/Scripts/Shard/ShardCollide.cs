﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShardCollide : MonoBehaviour {

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "player") { //if hit by the player
            Destroy(this.gameObject); //destroy this object
        }
    }
}
