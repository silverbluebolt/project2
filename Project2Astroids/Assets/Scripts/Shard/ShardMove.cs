﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShardMove : MonoBehaviour {

    private Transform tf; //variable to hold a transform component
    private GameObject player; //variable to hold the player
    private Vector3 direction; //variable to hold a vector 
    public float speed; //variable to control speed

	// Use this for initialization
	void Start () {
        //initialize variables
        tf = GetComponent<Transform>();
        player = GameObject.Find("PlayerShip(Clone)");
        direction = Random.insideUnitCircle; //set direction to a random vector inside of the unit circle
        direction.Normalize(); //edit direction to have a length equal to one
        speed = 0.5f;
        Destroy(this.gameObject, 2); //destroy this gameObject after two seconds
	}
	
	// Update is called once per frame
	void Update () {
        if (player == null) { //destroy this object if the player is destroyed
            Destroy(this.gameObject);
        }
        tf.position += direction * speed; //move in diection at the set speed
    }
}
