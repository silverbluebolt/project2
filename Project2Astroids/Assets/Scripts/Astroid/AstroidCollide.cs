﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidCollide : MonoBehaviour {
    
    public GameObject shard; //variable too hold the shard prefab

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Bullet") { //when hit by a bullet,
            GMScript.God.IncreaseScore(); //run increase score function
            GMScript.God.enemies--; //decriment the enemies counter
            Explode(); //run the explode function
            Destroy(this.gameObject); //destroy this astroid
        } else if (collision.gameObject.tag == "Player") { //when hit by a player,
            GMScript.God.enemies--; //decriment the enemies counter
            Destroy(this.gameObject); //destroy this astroid
        } 
    }

    private void Explode() {
        int pieces = Random.Range(2, 5); //generate a number of pieces
        for (int i = 0; i < pieces; i++) { //generate the pieces 
            Instantiate(shard, this.GetComponent<Transform>().position, this.GetComponent<Transform>().rotation);
        }
    }
}
