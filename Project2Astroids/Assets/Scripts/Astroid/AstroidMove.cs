﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidMove : MonoBehaviour {
    private Transform tf; //used to store transform component for position and movement
    private GameObject player; //used to store the player's gameObject and get position
    private Vector3 direction; //used to control the direction of movement and orientation of the sprite
    public float speed; //used to control linear speed

    // Use this for initialization
    void Start () {
        //initailize variables
        tf = GetComponent<Transform>();
        player = GameObject.Find("PlayerShip(Clone)");
        speed = 0.3f;

        direction = player.transform.position - tf.position; //find the direction to move/look by subtracting the current location from the player's location
        direction.Normalize(); //make direction vector one long
        tf.right = direction; //rotate to match direction       
	}
	
	// Update is called once per frame
	void Update () {
        if (player == null) {
            Destroy(this.gameObject); //destroy this object if the player dies
            GMScript.God.enemies--; //decriment the enemies counter in the GMScript 
        } else {
            tf.position += direction * speed; //move in direction at the speed chosen
        }
    }
}
