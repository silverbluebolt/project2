﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankContol : MonoBehaviour {

    Transform tf; // variable to hold the transform component
    public int rSpeed; // variable for rotation speed
    public float speed; // variable for linear speed

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>(); // store the transform component in a variable
        rSpeed = 3; //initialize the rotation speed
        speed = 0.4f; //initialize the linear speed
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.LeftArrow)) // if the left arrow is pressed
        {
            tf.Rotate(0, 0, rSpeed); // rotate the sprite counter-clockwise at rSpeed
        } else if (Input.GetKey(KeyCode.RightArrow)) // if the right arrow is pressed
        {
            tf.Rotate(0, 0, -rSpeed); // rotate the sprite clockwise at rSpeed
        }
        
        if (Input.GetKey(KeyCode.UpArrow)) // if the up arrow is pressed
        {
            tf.Translate(Vector3.right * speed, Space.Self); // move forward relative to the front of the sprite
        } else if (Input.GetKey(KeyCode.DownArrow)) //if the down arrow is pressed
        {
            tf.Translate(Vector3.left * speed, Space.Self); // move backwards relative to the front of the sprite
        }
	}
}
